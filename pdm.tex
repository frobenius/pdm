\documentclass[a4paper,10pt,showkeys,onecolumn,notitlepage]{revtex4-1}
\usepackage {CJK}
\usepackage[lmargin=.35in,rmargin=.35in,tmargin=1.in,bmargin=1in]{geometry}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{subfig}
\usepackage{textcomp}
\usepackage{color}
\usepackage{bm}
\usepackage{hyperref}
\usepackage{wrapfig}
\usepackage{pb-diagram}
\usepackage[dvipsnames]{xcolor}
\usepackage[makeroom]{cancel}
%\usepackage[sort&compress,numbers]{natbib}
\DeclareMathAlphabet{\mathcalligra}{T1}{calligra}{m}{n}

\pdfminorversion=6

\begin{document}


\title{Classical treatment of particle with position-dependent mass $m(r)=1/(1+r^4)$ in 1D and 2D subjected to harmonic potential}
\author{A.\,Khlevniuk}
\author{V.\,Tymchyshyn} \email{yu.binkukoku@gmail.com}
\affiliation{Bogolyubov Institute for Theoretical Physics National Academy of Sciences, Metrolohichna St. 14-b, Kyiv 03680, Ukraine}

\begin{abstract}
The purpose of this paper is to explore the motion of classical particle with PDM $m(r)=1/(1+r^4)$  in harmonic potential (1D and 2D).
We establish a geometric model of the particle with mentioned PDM that involves constant-mass particle in curved space.
With the help of Lagrangian formalism we solve equations of motion and express the result in terms of elliptic functions.
In general, we explore one of the possible developments of Mathews and Lakshmanan ideas and provide a new example of elliptic functions occurring in physics.
\end{abstract}
\keywords{position-dependent mass, lemniscatic oscillator, lemniscate of Bernoulli, rational parametrization.}
\maketitle

\section*{Introduction}

Studies of particles with position-dependent mass (PDM)  have gained vast popularity in the last decades due to the numerous physical problems they can appear within: nuclear physics, nanophysics, semiconductor research, etc.\cite{bastard1992wave,ring2004nuclear,keshavarz2013optical,ghosh2016influence}.
Many classical and quantum systems have been already explored \cite{y2007classical,mitra1978interaction,lin1970velocity}.
One of the most prominent examples is Mathews-Lakshmanan oscillator --- particle with PDM $m=1/(1+\lambda x^2)$ that moves in harmonic potential \cite{mathews1974unique}. 
In current contribution we present continuation of these ideas and consider a particle with PDM $m=1/(1+x^4)$ in harmonic potential.


It is known that PDM may have geometrical sense ---  point mass moving in curved space can be treated as a particle with PDM in Euclidean space.
A great example of such correspondence is given by Higgs \cite{higgs1979dynamical} and Mathews-Lakshmanan oscillators \cite{carinena2004non}.
Both mentioned models study a particle on a sphere, but provide different parametrizations to ``flatten'' the space and thus lead to different PDMs.
At the moment many different models of the kind are known.
For example, recent paper \cite{gap_sys} provides comprehensive classification of 1-dimensional PDMs with potentials that allow for quantum solvability.
In particluar, authors mention that for the point moving along the lemniscate of Bernoulli effective mass $m=1/(1+x^4)$ can be obtained.
We develop this model by first analytically solving the Bernoulli lemniscate model and then generalizing it to 2D.
Generalization is done by constructing appropriate surface in 4D space that leads to PDM $m=1/(1+r^4)$.


One more thing we want to emphasize.
Lemniscate curve is widely known in field of classical mechanics \cite{lemni_3,treatise_on_dynamics}, by no means least due to its interesting properties \cite{gravitational_prop}.
Despite many appearances of lemniscate and elliptic functions in different branches of physics \cite{primer_on_eliptic}, by this paper we want to add one more analytically solvable model to this list.


In conclusion, the  purpose of this paper is to solve analytically the 1D model outlined in \cite{gap_sys}, modify it with harmonic potential, and generalize it to 2D by constructing the appropriate curved space model.
We then solve the 2D model and highlight some of its properties.

\section{1D system}

Consider a point mass that moves along the lemniscate of Bernoulli (figure \ref{fig_lemniscate})
$$
    (X^2+Y^2)^2 = \frac{1}{2}(X^2-Y^2)
$$
and is subjected to the harmonic potential $U = \alpha^2 (X^2+Y^2)/2$ (e.g. the mass is attached to the origin with a stretchy rubber band).
The system is essentially one-dimensional, so we can use one of many 1D parametrizations of the lemniscate.
For example, {\it{natural}} parametrization
\begin{equation}
\label{natural_parametrization}
\begin{aligned}
X(s) &= \frac{1}{\sqrt{2}}\,
             \text{cn}\left( \sqrt{2}s \left| \frac{1}{2} \right.\right)
             \text{dn}\left( \sqrt{2}s \left| \frac{1}{2} \right.\right), \\
Y(s) &= \frac{1}{2}\,
             \text{cn}\left( \sqrt{2}s \left| \frac{1}{2} \right.\right)
             \text{sn}\left( \sqrt{2}s \left| \frac{1}{2} \right.\right),
\end{aligned}
\end{equation}
where $s$ is the length of the curve, is a solid choice in many cases, since it renders trivial metric tensor \cite{online_math_library}.


Despite of all advantages of natural parametrization, we use {\it{rational}} parametrization of the lemniscate \cite{gap_sys}
\begin{equation}
\label{rational_parametrization}
\begin{aligned}
    X(x) = \frac{1}{\sqrt{2}}\frac{x+x^3}{1+x^4}, \\
    Y(x) = \frac{1}{\sqrt{2}}\frac{x-x^3}{1+x^4},
\end{aligned}
\end{equation}
as it yields coordinate-mass dependence  $m=1/(1+x^4)$ we aim to explore.

\begin{figure}
     \centering
     \subfloat[a][]{
         \includegraphics[width=0.3\textwidth]{fig_1}
         \label{fig_lemniscate}
     }
     \subfloat[b][]{
         \includegraphics[width=0.3\textwidth]{fig_2}
         \label{potential_1d}
     }
     \subfloat[c][]{
         \includegraphics[width=0.3\textwidth]{fig_3}
         \label{fig_orbits_1d}
     }
    \caption{\textbf{(a)} Bead on a lemniscate. \textbf{(b)} Potential function $u=(1/2)m(x)x^2$. \textbf{(c)} Phase portrait for 1D system.}
    \label{1d_stuff}
\end{figure}


Parametrization \eqref{rational_parametrization} induces bounded and compact metric 
\begin{equation}
\label{metric_1d}
    \rho({x_1}, {x_2}) = \frac{|{x_1}-{x_2}| \sqrt{\strut 1 + x_1^2 x_2^2}   }
                              {\sqrt{\strut 1 + x_1^4}\sqrt{\strut 1 + x_2^4}},
\end{equation}
where $x_1, x_2 \in \mathbb{R}$.
The metric tensor
$$
    ds^2 = \frac{dx^2}{1 + x^4}
$$
causes preimage of classical unit-mass particle to acquire coordinate-dependent mass.
Similar approach was described in \cite{carinena2004non}, where certain parametrizations of the sphere and hyperbolic plane generate PDMs $1/(1 \pm x^2)$ and $1/(1 \pm x^2)^2$ (``$-$'' corresponds to the hyperbolic plane, ``$+$'' --- to the sphere).


If we make canonical transformation 
$$
    \mathcal{X}(x) = \int\sqrt{m(x)}dx=\int\frac{dx}{\sqrt{1+x^4}},
$$
as described in\cite{y2007classical}, we discover that parameter $\mathcal{X}(x)$ is actually arc length $s$ and hence we revert back to natural parametrization of the lemniscate \eqref{natural_parametrization}. 
For completeness here are formul\ae{} that connect $x$ and $s$
$$
\begin{aligned}
     \mathcal{X}(x) = s(x)&=\frac{1}{2}F\left(\arccos{\frac{1-x^2}{1+x^2}}\left|\frac{1}{2}\right.\right), \\
                                 x(s)&=\text{sn}(s|1/2)\cdot\text{dc}(s|1/2),
\end{aligned}
$$
where $F$ is elliptic integral of the first kind.

Under rational parametrization of the lemniscate, we get following expressions for kinetic and potential energy of the particle
$$
    T = \frac{1}{2} \left(\dot{X}^2+\dot{Y}^2\right)
       = \frac{1}{2} m(x)\dot{x}^2,\quad
    U = \frac{\alpha^2}{2}m(x)x^2,
$$
with $m(x) = 1/(1+x^4)$.
Note that harmonic potential preserves its form after pullback, that's one of the reasons we decided to explore this potential.
The Lagrangian of the system 
$$
L=T(x,\dot{x})-U(x)=\frac{1}{2}m(x)\dot{x}^2-\frac{\alpha^2}{2}m(x)x^2
$$
is invariant under substitution $x \rightarrow 1/x$.
We will use this symmetry to generate new solutions from the known ones.


Euler-Lagrange equation
\begin{equation}
\label{eq_euler_lagrange_d1}
    \ddot{x} - \frac{2x^3}{1+x^4}\dot{x}^2 + \alpha^2\frac{1-x^4}{1+x^4}x = 0
\end{equation}
classifies as quadratic Liénard type equation.
The system has integral of motion (energy)
$$
    E = \frac{1}{2}m(x)\dot{x}^2+\frac{\alpha^2}{2}m(x)x^2
$$
and can be solved
$$
    t - t_0 = \int{\frac{dx}{\sqrt{\frac{2}{m(x)}(E-U(x))}}},
$$
that is analogous to the one used for the particle of constant mass in classical mechanics.
Although the initial potential $\alpha^2 x^2/2$ is attractive, after parametrization it becomes repulsive for $|x|>1$.
This is due to the fact that under rational parametrization $\infty$ is ``glued'' to the origin.
That's why as long as the point passes the equator of the lemniscate, its attraction to the origin corresponds to the repulsion of its preimage.


For $\alpha \neq 0$ we use substitution $e = 4E/\alpha^2$ to switch to potential $(1/2) m(x)x^2$.
Depending on the energy $e$, there are either three $(e\leq1)$ solutions or one $(e>1)$ (refer to figure \ref{potential_1d} for more details).
If $e<1$ the turning points are $-b$, $-a$, $a$, $b$, where
$$
    a = \frac{\sqrt{1 - \sqrt{1-e^2}}}{\sqrt{e}},\quad 
    b = \frac{\sqrt{1 + \sqrt{1-e^2}}}{\sqrt{e}},
$$
satisfy the relation $ab=1$.
If $e<1$, we have  one bounded and two scattering (left and right) solutions:
\begin{equation}
\label{eq_sn_solution}
\begin{aligned}
    x_B(t) &=        a\, \text{sn}(\omega (t-t_0)| m), \\
    x_S(t) &= \pm b\, \text{ns}(\omega (t-t_0) | m),
\end{aligned}
\end{equation}
where
 $$
    \omega = \frac{\alpha}{\sqrt{2}} \sqrt{1+\sqrt{1-e^2}}, \quad
    m          = \frac{1-\sqrt{1-e^2}}{1+\sqrt{1-e^2}},
$$
and $t_0$ is chosen to satisfy initial conditions.
Subscripts in \eqref{eq_sn_solution} stand for $B$ --- ``Bounded'' and $S$ --- ``Scattering''.
Scattering (left and right) solutions are obtained by taking the multiplicative inverse of $\pm x_B(t)$ \eqref{eq_sn_solution} (as the Lagrangian \eqref{eq_euler_lagrange_d1} is invariant under $x$ inversion).
We will use this property again and again considering all the rest cases.
Note that amplitude $a$ and angular frequency $\omega$ satisfy the relation
$$
E=\frac{1}{2}\omega^2a^2=\frac{1}{2}\alpha^2\frac{a^2}{1+a^4}
$$
that is on a par with similar relation for Mathews-Lakshmanan oscillator \cite{mathews1974unique}.
Period of oscillation is $T=4K(m)/{\omega}$, where $K$ is the complete elliptic integral of the first kind.
If $e\ll 1$, bounded solution \eqref{eq_sn_solution} can be simplified to
$$
    x_B(t) = \sqrt{\frac{e}{2}} \sin(\alpha (t-t_0)),
$$
that is simple periodic motion we are used to see in the harmonic oscillator.


Let's consider the case $e=1$ ($m=1$).
Physically it describes a bead that has barely enough energy to reach the center of the bulb of the lemniscate (see figure \ref{fig_lemniscate}) or, in other words, it barely reaches the maximum of the potential (see figure \ref{potential_1d}).
Now solutions read as follows
$$
\begin{aligned}
    x_B(t) &= \tanh{(\omega (t-t_0))}, \\
    x_S(t) &= \pm \text{cth}{(\omega (t-t_0))}.
\end{aligned}
$$
Once again we have one bounded and two scattering solutions.


If $e>1$ we have the single unbounded solution 
$$
    x_U(t) = \text{sn}(w' (t-t_0)| m') \cdot \text{dc}(w' (t-t_0) | m'),
$$
where
$$
    w' = \sqrt{\frac{e}{2}}\alpha,\quad
    m' = \frac{1+e}{2 \, e}.
$$
This completes the case $\alpha \neq 0$.
The orbits are summarized in figure \ref{fig_orbits_1d} (phase portrait).
Please note that momentum is not directly proportional to the velocity, since mass depends on the coordinate.

If $\alpha=0$, the substitution we used is valid no more. In this case the system has one unbounded solution
$$
x(t) =       \text{sn}\left(\sqrt{2E} \, (t-t_0) \left| 1/2 \right. \right)
       \cdot \text{dc}\left(\sqrt{2E} \, (t-t_0) \left| 1/2 \right. \right).
$$


Obtained solutions have few interesting properties.
First of all, if particle's motion is unbounded, it will reach the infinity in a finite time determined by the period of the corresponding elliptic function.
It's not surprising though, because lemniscate is symmetric under horizontal reflection, but under rational parametrization its upper part is mapped to $]0,1[$, while the lower to $]1,\infty[$.
Thus if particle reaches zero in finite time, it will reach the infinity in finite time as well --- this effect is caused purely by the parametrization we used and by decreasing potential when $r\to \infty$.
The effect of finite escape time would not manifest itself though, if the effective potential function were increasing.
Interestingly, for $e=1$ the particle needs infinite time to reach $x=1$, but movement from any $x>1$ to $\infty$ will take finite time (though movement from $1$ to $x>1$ will take infinite time).
Note also that all solutions for $e\neq1$ are periodic, so after reaching the positive infinity the particle will reemerge from the negative one (and vice versa).
This corresponds to the full swing of the bead on the lemniscate. 
We can say that the particle moves in a double-well potential, but two halves of the second dimple are connected at point at infinity.
Thus particle in a sense ``oscillates through infinity'' for $e < 1$ or ``circles through infinity'' for $e > 1$.
Note also that pdm $m(x)=1/(1+x^4)$ vanishes at infinity.


Another thing to mention is that scattering and bounded solutions can be interconverted by changing $x(t) \rightarrow 1/x(t)$.
Also, solution \eqref{eq_sn_solution} with complex modulus and frequency can be used to infer the other two ($e > 1$ and $e = 1$) solutions with real parameters.

Obtained expressions resemble solutions that describe movement of point mass in quartic potential \cite{primer_on_eliptic}.
It's worth noting that for chosen PDM many potential functions can be solved in terms of elliptic integrals, for example
$$ 
    U(x) = \frac{\alpha^2}{2}m(x) \left(\frac{c_1}{x^2}+c_2+c_3x^2+c_4x^4+c_5x^6 \right)
    \quad \text{and} \quad
    U(x) = \frac{\alpha^2}{2}\left(\frac{c_1}{x^2}+c_2+c_3x^2 \right).
$$


\section{Generalization to 2D}

We generalize 1D system to 2D by generalizing its metric function \eqref{metric_1d} to $\mathbb{R}^2$ 
\begin{equation}
\label{lemni_metric_2d}
\rho(\bm{r_1}, \bm{r_2}) = \frac{|\bm{r_1}-\bm{r_2}| \sqrt{\strut 1+|\bm{r_1}|^2 |\bm{r_2}|^2}}{\sqrt{\strut 1+|\bm{r_1}|^4}\sqrt{\strut 1+|\bm{r_2}|^4}},
\end{equation}
where $\bm{r_1},\bm{r_2} \in \mathbb{R}^2$ with the metric tensor 
$$
ds^2= \frac{\bm{dr}^2}{1+{r}^{4}}.
$$
As for 1D, this metric is bounded and compact.
Metric \eqref{lemni_metric_2d} is the pullback of the $\mathbb{R}^4$-Euclidean metric for the surface parametrized by
\begin{equation}
\begin{aligned}
\label{surf_2d}
X_1(r,\phi)&=\frac{1}{\sqrt{2}} \frac{r+r^3}{1+r^4} \cos{\phi}; \quad  X_3(r,\phi)=\frac{1}{\sqrt{2}} \frac{r-r^3}{1+r^4} \cos{\phi}; \\
X_2(r,\phi)&=\frac{1}{\sqrt{2}} \frac{r+r^3}{1+r^4} \sin{\phi}; \quad X_4(r,\phi)=\frac{1}{\sqrt{2}} \frac{r-r^3}{1+r^4} \sin{\phi}.
\end{aligned}
\end{equation}
Here $r \geq 0$ is the radius and $0 \leq \phi < 2\pi$ ---  polar angle in $\mathbb{R}^2$.
The locus of points in $\mathbb{R}^4$, we get under this map, is the subset of the 4D lemniscate of Bernoulli that satisfies equation
\begin{equation}
\label{lemni_4d}
    \left(X_1^2+X_2^2+X_3^2+X_4^2\right)^2 = \frac{1}{2}\left(X_1^2+X_2^2-X_3^2-X_4^2\right),
\end{equation}
with $(X_1, X_2, X_3, X_4)\in \mathbb{R}^4$.

\begin{figure}
     \centering
     \subfloat[a][]{
         \includegraphics[width=0.4\textwidth]{fig_4}
         \label{fig_decision_tree}
     }   
	\hspace {0.1\textwidth} 
     \subfloat[b][]{
         \includegraphics[width=0.3\textwidth]{fig_5}
         \label{per_4}
     }  
     \caption{\textbf{(a)} Decision tree on how many solutions (turning points) effective potential $ 1/\left(2m(r)r^2\right)+\lambda^2 m(r)r^2/2$ allows for. \textbf{(b)}~Unclosed trajectory ($\epsilon=3.2$, $\lambda = 0$).}
    \label{2d_stuff}
\end{figure}


Consider the point mass that moves along 2-parameter surface in 4D defined by \eqref{surf_2d} and is attached to the origin with the ``rubber band", so it is subjected to the harmonic potential $U=(\alpha^2/2)(X_1^2+X_2^2+X_3^2+X_4^2)$.
Then the 2D preimage of the point with polar coordinates $(r, \phi)$ acquires mass $m(r)=1/(1+r^4)$ and the potential energy function pullbacks to $(\alpha^2/2)m(r)r^2$.
Its Lagrangian
$$
L(r,\dot{r},\dot{\phi})=T(r,\dot{r},\dot{\phi})-U(r)=\frac{m(r)}{2}\left( \dot{r}^2+r^2\dot{\phi}^2\right)-\frac{\alpha^2}{2}m(r)r^2
$$
is invariant under radius inversion.
Also it could be rewritten in terms of $r^2$ and $\phi$.
Euler-Lagrange equations are the following: 
\begin{equation}
\begin{aligned}
&\ddot{r}-\frac{2 r^3}{1+r^4}\dot{r}^2-r\frac{1-r^4}{1+r^4}\dot{\phi}^2+\alpha^2 \frac{1-r^4}{1+r^4}r=0, \\
&\ddot{\phi}+\frac{2}{r}\frac{1-r^4}{1+r^4}\dot{r}\dot{\phi}=0.
\end{aligned}
\end{equation}
If $\alpha = 0$ these equations describe geodesics for \eqref{lemni_metric_2d}.


The system has two classical integrals of motion: energy and angular momentum
$$
\begin{aligned}
E &= \frac{m(r)\dot{r}^2}{2}+\frac{m(r)r^2\dot{\phi}^2}{2}+U(r)=\frac{m(r)\dot{r}^2}{2}+\frac{L^2}{2m(r)r^2}+\frac{\alpha^2}{2}m(r)r^2,\\
L &= m(r)r^2\dot{\phi}.
\end{aligned}
$$
If $\alpha = 0$  the effective potential is identical to the potential of the constant-mass isotonic oscillator.
The general solution is provided by classical formul\ae \cite{landau}:
\begin{equation}
\label{euler_2d}
\begin{aligned}
&t-t_0=\int\frac{dr}{\sqrt{\strut \frac{2}{m(r)}(E-U(r))-\frac{L^2}{m^2(r)r^2}}}, \\
&\phi-\phi_0=\int \frac{L\, dt}{m(r)r^2(t)}.
\end{aligned}
\end{equation}
In order to get turning points we solve equation $E = \frac{L^2}{2m(r)r^2}+\frac{\alpha^2}{2}m(r)r^2$. 


After substitution $\lambda=\alpha/L$, $\epsilon=E/L^2$ ($L\neq 0$) we get equation
\begin{equation}
\label{eq_eps}
    \epsilon = \frac{1}{2m(r)r^2}+\frac{\lambda^2}{2}m(r)r^2=\frac{1}{2}\left(r^2+\frac{1}{r^2}\right)+\frac{\lambda^2}{2}\frac{1}{r^2+\frac{1}{r^2}}.
\end{equation}
We define $\delta = (1/2)\left(r^2+1/r^2\right)$ and solve \eqref{eq_eps} with respect to it
$$
\delta_1=\frac{1}{2}(\epsilon+\sqrt{\epsilon^2-\lambda^2}); \quad
\delta_2=\frac{1}{2}(\epsilon-\sqrt{\epsilon^2-\lambda^2}).  \qquad
$$
By solving $\delta_{1,2}$ for $r^2$ we get
$$
a=\delta_1+\sqrt{\delta_1^2-1}; \quad b=\delta_2+\sqrt{\delta_2^2-1};
$$
$$
d=\delta_1-\sqrt{\delta_1^2-1}; \quad c=\delta_2-\sqrt{\delta_2^2-1},
$$
that satisfy relation $ad=bc=1$.
Points $\sqrt{a}, \sqrt{b}, \sqrt{c}, \sqrt{d}$ are turning points for the particle with energy $\epsilon$.
For $\lambda \leq 2$ the effective potential is U-shaped, while for $\lambda > 2$ it's W-shaped.
If $\lambda \leq 2$ there can be two turning points ($\epsilon > \lambda$), one ($\epsilon = \lambda$)  or none ($\epsilon < \lambda$).
If $\lambda > 2$ and $1+\lambda^2/4 \leq \epsilon \leq \lambda$ we have 4 turning points.
If $\lambda > 2$ and $\epsilon > \lambda$ there are two turning points and none of them if  $\epsilon < 1+\lambda^2/4$.
Possibilities are summarized in figure \ref{fig_decision_tree} (see also figure \ref{potent_2} for more intricate $\lambda > 2$ case).


Integrals \eqref{euler_2d} can be expressed in terms of Jacobi elliptic functions and elliptic integral of the third kind \cite{gradstein}.
If $\lambda =0$ (free particle) the solution is
\begin{equation}
\label{free_particle_solution}
\begin{aligned}
r^2(\tau)&=\frac{\text{sn}^2(\tau|m) \text{dc}^2(\tau|m)+d}{d ~ {\text{sn}}^2(\tau|m) {\text{dc}}^2(\tau|m)+1}, \\
\phi(\tau)-\phi_0&=\frac{1}{\omega}\big[-2 \tau+\Pi_0 \, \Pi(m';\text{am}(2 \tau| m)| m))\big],
\end{aligned}
\end{equation}
where $\Pi(\cdots)$ is the elliptic integral of the third kind.
Note that 
$$
\Pi(m';\text{am}(2 \tau| m)| m)) = \int_0^{2\tau}\frac{dw}{1-m' \text{sn}^2(w|m)}.
$$
Parameters that are involved in the solution can be expressed in terms of either ``geometric''  ($a, b, c, d$) or ``energy'' ($\epsilon$ and $\lambda$) variables.
Here and further we take the hybrid approach choosing whatever results in simplest expressions
$$
          \tau =\omega L (t-t_0); 
\quad \omega=\sqrt{a+d}=\sqrt{2\, \epsilon};
\quad \Pi_0=1+\epsilon; 
\quad m'=\frac{1-\epsilon}{2};
\quad m=\frac{\epsilon-1}{2 \, \epsilon}.
$$
In general orbits are not closed (fig.~\ref{per_4}), since functions $r(\tau)$ and $\phi(\tau)$ are both periodic and their periods match for certain energies only.
Actually $r(\tau)$ has period $2nK(m)$, where $K(m)$ is quarter period of Jacobi elliptic functions and $n$ is positive integer.
Note that $m$ depends on energy $\epsilon$.
The equation that expresses periodicity of $\phi$ then reads $\phi(2nK(m))=\phi(0)+2\pi k$, where $k$  is integer, and can be solved numerically for $\epsilon$ (for example, see figures \ref{per_1}, \ref{per_2}, \ref{per_3}).
Obtained closed trajectories resemble \textit{regular star polygons} in Euclidean space.


Pericenter distance is always the inverse of the apocenter distance hence, as $\epsilon\to \infty$, the pericenter decreases and the apocenter increases.
Also, the movement of the particle for $r>1$ is just a mirrored (with respect to circle $r=1$) image of its movement for $r<1$. 
If $\epsilon=1$  the point moves in circular orbit of radius $r=1$ with constant angular speed.


\begin{figure}
     \centering
     \subfloat[][Fourfold symmetry ($\epsilon=1.62, \lambda=0)$.]{
         \includegraphics[width=0.3\textwidth]{fig_6_1}
         \label{per_1}
     }
     \subfloat[][Sevenfold symmetry ($\epsilon=1.1, \lambda=0)$.]{
         \includegraphics[width=0.3\textwidth]{fig_6_2}
         \label{per_2}
     }
     \subfloat[][Sevenfold symmetry ($\epsilon=6.4, \lambda=0$).]{
         \includegraphics[width=0.3\textwidth]{fig_6_3}
         \label{per_3}
     }
     \caption{Some of the closed trajectories for $\lambda = 0$ with different symmetries and energies $\epsilon$.}
     \label{fig_closed_trajectories}
\end{figure}


Now let us give general solution for $\lambda \neq 0$ and $\epsilon > 1 + \lambda^2 / 4$:
\begin{equation}
\label{non_free_solution}
\begin{aligned}
r^2(\tau)&=\frac{\text{sn}^2(\tau|m) \text{dc}^2(\tau|m)+d}{d ~ {\text{sn}}^2(\tau|m) {\text{dc}}^2(\tau|m)+1}, \\
\phi(\tau)-\phi_0&=\frac{1}{\omega}\big[-2\tau+\Pi_0 \, \Pi(m';\text{am}(2 \tau| m)| m))\big],
\end{aligned}
\end{equation}
where
$$
          \tau =\omega L (t-t_0);
\quad \omega=\sqrt{(a+d)-(b+c)}=\sqrt{2(\delta_1-\delta_2)};
\quad \Pi_0=1+\delta_1;  \quad m'=\frac{1-\delta_1}{2};
\quad m=\frac{(\delta_1-\delta_2)+\delta_1 \delta_2-1}{2 \, (\delta_1-\delta_2)}.
$$
Note that $\delta_1 \rightarrow \epsilon$, $\delta_2 \rightarrow 0$ as $\lambda \rightarrow 0$ and therefore solution \eqref{non_free_solution} reduces to \eqref{free_particle_solution}.

\begin{figure}
     \centering
     \subfloat[][Left solution ($\epsilon = 195.6, \lambda = 2.5$).]{
         \includegraphics[width=0.3\textwidth]{fig_7_1}
         \label{left_rose}
     }
     \subfloat[][Effective potential  $u = 1/(2m(r)r^2) + \lambda^2 m(r) r^2 / 2$, $(\lambda>2)$.]{
         \includegraphics[width=0.3\textwidth]{fig_7_2}
         \label{potent_2}
     }
     \subfloat[][Right solution ($\epsilon =195.6, \lambda=2.5$).]{
         \includegraphics[width=0.3\textwidth]{fig_7_3}
         \label{right_rose}
     }
    \caption{Solutions for $2 < \lambda \leq \epsilon \leq 1+\lambda^2/4$.}
    \label{fig_left_right_trajectories}
\end{figure}



If $2 < \lambda \leq \epsilon \leq 1+\lambda^2/4$ there are two solutions (4 turning points) that we call left and right (see figures \ref{left_rose},\ref{right_rose}).
Left solution can be expressed as
\begin{equation}
\begin{aligned}
\label{eq_left_solution}
r_{L}^2(\tau)&=\frac{d\,(a-c)+a\,(c-d) \, \text{sn}^2(\tau|m)}{(a-c)+(c-d)\, \text{sn}^2(\tau|m)}, \\
\phi_{L}(\tau)-\phi_0&=\frac{1}{\omega}\big[\Omega_0 \tau-\Pi_0\left(\Pi\left(m'_1;\text{am}(\tau|m)|m\right)-\Pi\left(m'_2;\text{am}(\tau|m)|m\right)\right)\big],
\end{aligned}
\end{equation}
where
$$
          \tau=\omega L (t-t_0);
\quad \Omega_0=a+d; 
\quad \Pi_0=a-d;
\quad m'_1=\frac{d-c}{a-c}; 
\quad m'_2=\frac{a}{d}\frac{d-c}{a-c};
$$
$$
    \omega = \sqrt{(a-c)(b-d)}
                 = \sqrt{2} \, \sqrt{-1 + \lambda^2/4+\sqrt{\left(1 + \lambda^2/4\right)^2 - \epsilon^2}};
$$
$$
    m = \frac{(a-b)(c-d)}{(a-c)(b-d)}
        =\frac{-1 + \lambda^2/4 - \sqrt{(1 + \lambda^2/4)^2 - \epsilon^2}}
                  {-1 + \lambda^2/4 + \sqrt{(1 + \lambda^2/4)^2 - \epsilon^2}}.
$$
Right solution can be obtained from the left one by changing $a$ to $d$ and $b$ to $c$ or by substitution $r(\tau) \rightarrow 1/r(\tau)$.
This means that the particle oscillates in both dimples with the same frequency.
If $\epsilon=\lambda$  we have two possible circular trajectories with constant angular speed: $r=\sqrt{c}=\sqrt{d}$ and $r=\sqrt{a}=\sqrt{b}$.
If $L\neq 0$ the motion is always finite. If $L=0$ we effectively have 1D case that we discussed earlier. 


It's well known \cite{bateman1953higher} that elliptic functions are intimately connected with modular group and M\"obius group of transformations of the complex plane of the form:
$$
z \rightarrow \frac{Az+B}{Cz+B}.
$$
Many formul\ae{} in the presented article can be reformulated with the help of M\"obius transformation theory and interpreted in terms of poles, cross-ratios, characteristic parallelogram, etc. 
For example, recall that in 1D we essentially had two types of solutions: unbounded solutions expressed in terms of $\text{sn}\cdot\text{dc}$ and bounded solutions involving $\text{sn}$ function. 
Expressions for $r^2(\tau)$ in \eqref{non_free_solution} and \eqref{eq_left_solution} are in fact M\"obius transforms of squares of these solutions.

\section*{Conclusion}

In this article we considered classical particle with PDM $m(x)=1/(1+x^4)$ in harmonic potential $(\alpha^2/2)m(x)x^2$.
As was shown, this PDM can be generated by the specific pullback metric.
It can be treated as development of ideas of Cari\~{n}ena et all \cite{carinena2004non}, who has shown that parametrization of sphere and hyperbolic plane can generate PDMs $1/(1 \pm x^2)$ and $1/(1 \pm x^2)^2$ ($-$ corresponds to hyperbolic plane, $+$ --- to sphere).
Besides, we generalized this PDM to 2D by finding appropriate pullback metric.
It's worth noting that we used a method different from the one Mathews-Lashmanan oscillator was generalized in \cite{carinena2004non}. As a result $m(r)r^2\dot{\phi}$ is conserved instead of $r^2\dot{\phi}$.


Using Lagrangian formalism we provided analytic solutions for 1D and 2D cases.
Solutions can be written in terms of Jacobi elliptic functions (1D) and Jacobi elliptic functions and elliptic integrals of the third kind (2D in polar coordinates).
Interestingly, bounded solutions for 1D show specific dependence between amplitude and angular frequency that is on a par with similar relation for Mathews-Lakshmanan oscillator \cite{mathews1974unique}.


\bibliographystyle{apsrev4-1}
\bibliography{pdm_bib}

\end{document}