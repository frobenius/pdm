﻿
Dear Editor,

we noted, you have probably encountered some issues while securing reviewers.
We would like to provide assistance and add to the list of suggested reviewers following experts:

1. Lakshmanan Muthusamy (Centre for Nonlinear Dynamics, Department of Physics, Bharathidasan University, Tirachirappalli, India) lakshman@cnld.bdu.ac.in  lakshman.cnld@gmail.com,
2. Sara Cruz y Cruz (Instituto Politecnico Nacional, UPIITA (Mexico))   sara@fis.cinvestav.mx
3. Bohdan I. Lev (M. M. Bogolyubov Institute for Theoretical Physics, Kiev, Ukraine) blev@bitp.kiev.ua bohdan.lev@gmail.com
4. Leonardo Casetta (Escola Politécnica, University of São Paulo, São Paulo, Brazil) lecasetta@gmail.com

On behalf of authors,
Tymchyshyn V.
